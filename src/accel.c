/**
 * @file
 * @brief File containing all accelerometer related logic (setup, reading, interaction with the library)
 */
#include "accel.h"

static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);

/**
 * @brief Setup of the double tap functionality.
 *
 * Sets suitable values for the threshold etc. and enables the detection for all axes.
 */
void setup_double_tap_detection() {
    // Keep interrupt high until register is read
    lis3dh_tap_notification_mode_set(&dev_ctx, LIS3DH_TAP_LATCHED);
    // Set click threshold to 12h -> 0.281 g (1 LSB = full scale/128)
    lis3dh_tap_threshold_set(&dev_ctx, 0x20);
    // Set TIME_LIMIT (wie lange ein einzelner Klick maximal sein darf) to 20h -> 80 ms (1 LSB = 1/ODR)
    lis3dh_shock_dur_set(&dev_ctx, 0x30);
    // Set TIME_LATENCY (minimale wartezeit zwischen zwei Klicks?) to 20h -> 80 ms
    lis3dh_quiet_dur_set(&dev_ctx, 0x30);
    // Set TIME_WINDOW  (gesamtes zeitfenster für beide klicks?) to 30h -> 120 ms
    lis3dh_double_tap_timeout_set(&dev_ctx, 0x50);

    //Enable double click detection on all axis
    lis3dh_click_cfg_t click_cfg;
    lis3dh_tap_conf_get(&dev_ctx, &click_cfg);
    click_cfg.xd = PROPERTY_ENABLE;
    click_cfg.yd = PROPERTY_ENABLE;
    click_cfg.zd = PROPERTY_ENABLE;
    lis3dh_tap_conf_set(&dev_ctx, &click_cfg);

    // Apply high pass to double click detection
    lis3dh_ctrl_reg2_t ctrl_reg2;
    lis3dh_read_reg(&dev_ctx, LIS3DH_CTRL_REG2, (uint8_t *) &ctrl_reg2, 1);
    ctrl_reg2.hp = 0b100;
    lis3dh_write_reg(&dev_ctx, LIS3DH_CTRL_REG2, (uint8_t *) &ctrl_reg2, 1);
    lis3dh_read_reg(&dev_ctx, LIS3DH_CTRL_REG2, (uint8_t *) &ctrl_reg2, 1);
}

#define MIN_MS_BETWEEN_CONSECUTIVE_CLICKS 1000
#define MIN_MS_BETWEEN_POLLS 500
uint32_t last_poll_ms = 0;
uint32_t last_click_ms = 0;
/**
 * @brief Fetches the tap detection register and checks whether the user has tapped with debounce.
 *
 * Only works if ::setup_double_tap_detection has been called previously.
 *
 * Also prevents the sensor from being fetched too often. Fetching it too often results into undefined behaviour of the sensor.
 *
 * @return True if the user has tapped on the accelerometer since the last call to this function, else false.
 */
bool has_double_tapped() {
    uint32_t current_ms = get_current_ms();
    if ((current_ms - last_poll_ms) > MIN_MS_BETWEEN_POLLS) {
        lis3dh_click_src_t src;
        /* Check double tap event */
        lis3dh_tap_source_get(&dev_ctx, &src);
        if (src.dclick && ((current_ms - last_click_ms) > MIN_MS_BETWEEN_CONSECUTIVE_CLICKS)) {
            // Non-blocking debouncing
            last_click_ms = current_ms;
            return true;
        }
        last_poll_ms = current_ms;
    }
    return false;
}

/**
 * @brief Checks if new accelerometer values are available and returns them as a single absolute value in that case.
 *
 * @param result It will be updated with sum of all absolute accelerometer values in mg. Only use the values when function returned true.
 * @return Whether new values were available. Do not use the values of results when false is returned.
 */
bool get_new_accel_abs(uint16_t* result) {
    int16_t raw_accel[3] = { 0 };

    lis3dh_reg_t reg;
    lis3dh_xl_data_ready_get(&dev_ctx, &reg.byte);
    if (reg.byte) {
        // Read accelerometer data only if new value available
        lis3dh_acceleration_raw_get(&dev_ctx, raw_accel);
        *result = 0;
        for (int i = 0; i < 3; i++) {
            *result += abs(raw_accel[i]);
        }
        // Convert raw accelerometer values to the values in mg for our input settings (highest possible res)
        *result = *result / 16;
        return true;
    }
    return false;
}

/**
 * @brief Setup code for the LID3DH accelerometer
 *
 * - Initialize ::dev_ctx with the correct I2C read and write functions
 * - Setup highest possible resolution and refresh rate
 * - Enable high-pass on the output to remove the gravity offset from the values
 */
void accel_setup() {
    /* Initialize mems driver interface */
    dev_ctx.write_reg = platform_write;
    dev_ctx.read_reg = platform_read;
    dev_ctx.handle = NULL;

    static uint8_t whoamI;
    /* Check device ID */
    lis3dh_device_id_get(&dev_ctx, &whoamI);

    if (whoamI != LIS3DH_ID) {
        while (1) {
            /* Unknown device connected, should never happen */
        }
    }
    //while(1);
    /* Enable Block Data Update. */
    lis3dh_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
    /* Set Output Data Rate to 1Hz. */
    lis3dh_data_rate_set(&dev_ctx, LIS3DH_ODR_400Hz);
    /* Set full scale to 2g. */
    lis3dh_full_scale_set(&dev_ctx, LIS3DH_2g);
    /* Set device in continuous mode with 12 bit resol. */
    lis3dh_operating_mode_set(&dev_ctx, LIS3DH_HR_12bit);

    // Enable Highpass to remove offset
    lis3dh_high_pass_on_outputs_set(&dev_ctx, 1);
    lis3dh_high_pass_bandwidth_set(&dev_ctx, LIS3DH_MEDIUM);
}

/**
 * @brief This is a mapper function between the LIS3DH library and the provided i2c library for writing to a register of the sensor.
 */
static int32_t platform_write(__attribute__((unused)) void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {

    // Autoincrement register
    reg |= 0x80;
    // Add registry address as first value to write (it's the I2C subaddress)
    uint8_t write_buffer[len + 1];
    write_buffer[0] = reg;
    memcpy(write_buffer + 1, bufp, len);
    bool ret = i2c_transfer(LIS3DH_I2C_ADD_H >> 1u, write_buffer, len + 1, NULL, 0);
    return ret ? 0 : -1;
}

/**
 * @brief This is a mapper function between the LIS3DH library and the provided i2c library for reading a register of the sensor.
 */
static int32_t platform_read(__attribute__((unused)) void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
    // Autoincrement register
    reg |= 0x80;
    // Start a read transfer by first writing the read address to the bus
    // and then receive the content of it into the provided bufp
    bool ret = i2c_transfer(LIS3DH_I2C_ADD_H >> 1u, &reg, 1, bufp, len);
    return ret ? 0 : -1;
}

