/**
 * @file
 */
#include "clock.h"
#include "delay.h"
#include "lcd.h"
#include "accel.h"
#include "eeprom.h"
#include "systick.h"

#include <stm32l073xx.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static char tx_buffer[1000];

/**
 * @brief The smallest aggregation. This number of values will be aggregated by only considering the maximum.
 */
#define VALUES_TO_AGGREGATE_WITH_MAX 400
/**
 * @brief How many of the max-aggregated values (::VALUES_TO_AGGREGATE_WITH_MAX) should be considered for calibration.
 */
#define CALIBRATION_VALUES 10
/**
 * @brief How many of the max-aggregated values (::VALUES_TO_AGGREGATE_WITH_MAX) should be considered for aggregating them into flash.
 */
#define LONG_AGGREGATE_VALUES 20
#define INITIAL_CALIBRATION_VALUE -10000

static uint16_t aggregated_value_counter;
static int16_t max_in_aggregation;

static int16_t calibration_counter;
static int16_t calibration_buffer[CALIBRATION_VALUES];
static int16_t calibration_value;

static int32_t store_value;
static int16_t long_aggregetion_average;
static uint8_t store_counter;

/**
 * @brief Configure the relevant buttons (D3, D4) as input
 */
void setup_buttons(void) {
    // Enable RCC clock for register B
    RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
    // Set input mode for GPIO B pin3+5
    GPIOB->MODER &= ~GPIO_MODER_MODE3;
    GPIOB->MODER &= ~GPIO_MODER_MODE5;
    // No pull-up pull-down mode for both pins
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD3;
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD5;
}

/**
 * @return true if the download button (white, button 1) is pressed, else false.
 */
bool download_button_pressed(void) {
    return !(GPIOB->IDR & GPIO_IDR_ID3);
}

/**
 * @return true if the record button (red, button 2) is pressed, else false.
 */
bool record_button_pressed(void) {
    return !(GPIOB->IDR & GPIO_IDR_ID5);
}

/**
 * @brief Main function for processing, aggregating and calibrating the accelerometer values.
 *
 * 1. Aggregates all values within a short time period of ::VALUES_TO_AGGREGATE_WITH_MAX (usually ~ 1s).
 *    Aggregation is done based on calculating the maximum received in that time period.
 * 2. Again aggregate those max-aggregated values into bigger chunks of ::LONG_AGGREGATE_VALUES (usually 5-30).
 *    Values are aggregated by calculating the average of all the previously aggregated maximums.
 *    Those values are then also stored in EEPROM by calling the respective functions.
 *
 * The function also performs a calibration by collecting a number of ::CALIBRATION_VALUES valid values.
 * It then calculated the average of those calculated values.
 * This average offset will then be subtracted from all values in the future.
 *
 * @param accel_value The new value as the absolute sum of all axes.
 */
void process_new_accel(int16_t accel_value) {
    if (accel_value > max_in_aggregation) {
        max_in_aggregation = accel_value;
    }
    aggregated_value_counter++;
    // Aggregated Values for 1 second
    if (aggregated_value_counter == VALUES_TO_AGGREGATE_WITH_MAX) {
        store_counter++;

        if (calibration_value != INITIAL_CALIBRATION_VALUE) {
            // In calibrated state
            max_in_aggregation -= calibration_value;
            store_value += max_in_aggregation;
            if (store_counter == LONG_AGGREGATE_VALUES) {
                long_aggregetion_average = store_value / store_counter;
                write_flash(long_aggregetion_average);
                store_value = 0;
                store_counter = 0;
            }
            sprintf((char *) tx_buffer, "max:%5d       AGG:%5d", max_in_aggregation, long_aggregetion_average);
        } else {
            // Not in calibrated state
            sprintf((char *) tx_buffer, "max:%5d       CAL INCOMING", max_in_aggregation);
            if (store_counter > 4) {
                // Not in calibrated state and waited for an initial time
                sprintf((char *) tx_buffer, "max:%5d       CAL", max_in_aggregation);
                calibration_buffer[calibration_counter] = max_in_aggregation;
                calibration_counter++;

                if (calibration_counter == CALIBRATION_VALUES) {
                    calibration_value = 0;
                    for (int i = 0; i < CALIBRATION_VALUES; i++) {
                        calibration_value += calibration_buffer[i];
                    }
                    // calculate mean
                    calibration_value = calibration_value / CALIBRATION_VALUES;
                    sprintf((char *) tx_buffer, "CAL FIN");
                    store_counter = 0;
                    // Reset flash only now, so the user has time until the calibration is finished to cancel the recording
                    flash_reset();
                }
            }
        }

        lcd_return_home();
        lcd_print_string(tx_buffer);

        // reset pointer
        aggregated_value_counter = 0;
        max_in_aggregation = 0;
    }
}

/**
 * @brief Reset all recording related values to their initial state and clears the EEPROM (pointer).
 */
void init_recording() {
    calibration_value = INITIAL_CALIBRATION_VALUE;
    aggregated_value_counter = 0;
    max_in_aggregation = 0;
    calibration_counter = 0;
    store_counter = 0;
    long_aggregetion_average = 0;
}

/**
 * @brief Poll, aggregate, and store the values as long as the user hasn't pressed the red button again
 *
 * Also handles double taps on the display, which'll "disable" the LCD and enable it on a repeated double tap.
 * Unfortunately the background lighting can't be disabled for this LCD model, so only the text won't be displayed...
 */
void record_loop() {
    sprintf((char *) tx_buffer, "Record starting");
    lcd_return_home();
    lcd_print_string(tx_buffer);
    // Debounce button
    delay_ms(1500);

    // Clear any pending double tap interrupt
    has_double_tapped();
    // Reset all variables to initial state
    init_recording();
    uint8_t click_counter = 0;
    while (1) {
        uint16_t abs_accel = 0;
        if (get_new_accel_abs(&abs_accel)) {
            process_new_accel(abs_accel);
        }

        if (has_double_tapped()) {
            if (click_counter % 2 == 0) {
                lcd_disable();
            } else {
                lcd_init();
            }
            click_counter++;
        }
        if (record_button_pressed()) {
            break;
        }
    }
}

/**
 * @brief Responsible for displaying the user menu
 *
 * Let's the user select either the download or recording function. Handles button debouncing.
 *
 * Returns from this function as soon as the user exits one of the sub-functions.
 */
void select_menu() {
    lcd_init();
    sprintf((char *) tx_buffer, "RED=RECORD      WHITE=SAVE");
    lcd_return_home();
    lcd_print_string(tx_buffer);
    while (1) {
        if (download_button_pressed()) {
            sprintf((char *) tx_buffer, "Saving to USART");
            lcd_return_home();
            lcd_print_string(tx_buffer);
            uint16_t flash_len = download_flash_to_usart();
            if (flash_len == 0) {
                sprintf((char *) tx_buffer, "Nothing to save...");
            } else {
                sprintf((char *) tx_buffer, "%d values downloaded", flash_len);
            }
            lcd_return_home();
            lcd_print_string(tx_buffer);
            // Debounce
            delay_ms(2000);
            break;
        }

        if (record_button_pressed()) {
            record_loop();
            sprintf((char *) tx_buffer, "Rec stopped...");
            lcd_return_home();
            lcd_print_string(tx_buffer);
            // Debounce
            delay_ms(2000);
            break;
        }
    }

}

int main(void) {
    clock_setup_16MHz();
    systick_setup();
    i2c_setup();
    lcd_init();
    flash_setup();
    accel_setup();
    usart_setup();
    setup_double_tap_detection();
    setup_buttons();

    lcd_print_string("    Embedded    "
            "    Systems!    ");

    delay_ms(1000);
    while (1) {
        select_menu();
    }
}
