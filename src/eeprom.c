/**
 * @file
 * @brief File containing all EEPROM/flash related logic for setup, storing and reading over USART.
 */
#include "eeprom.h"

/**
 * @brief Pointer to the EEPROM start value.
 */
static int16_t * eeprom_base_ptr = (int16_t *) (DATA_EEPROM_BASE);
/**
 * @brief Contains the next allowed write position inside the EEPROM (last write position + 1).
 */
static uint16_t data_write_counter;

#define DATA_ARRAY_SIZE (DATA_EEPROM_BANK2_END-DATA_EEPROM_BASE)/sizeof(eeprom_base_ptr[0]) - 2
#define FLASH_COUNTER_POINTER DATA_ARRAY_SIZE + 1

/**
 * @brief Unlock the EEPROM for writing and setup the write pointer
 * Source: data sheet appendix
 */
void flash_setup() {

#define FLASH_PEKEY1               ((uint32_t)0x89ABCDEFU) /*!< Flash program erase key1 */
#define FLASH_PEKEY2               ((uint32_t)0x02030405U) /*!< Flash program erase key: used with FLASH_PEKEY2
                                                               to unlock the write access to the FLASH_PECR register and
                                                               data EEPROM */
    while ((FLASH->SR & FLASH_SR_BSY) != 0)
        //Wait till no operation is on going
        ;
    if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0) {   //Check if the PELOCK is unlocked
        FLASH->PEKEYR = FLASH_PEKEY1;   //Perform unlock sequence
        FLASH->PEKEYR = FLASH_PEKEY2;
    }
    data_write_counter = eeprom_base_ptr[FLASH_COUNTER_POINTER];
}

/**
 * @brief Write value to the EEPROM as long as it isn't full.
 * ::data_write_counter is also stored in a special position (towards the end of the EEPROM)
 * and it tracks how far the EEPROM has been written for reading it out later.
 *
 * @param value 2 Byte integer value to write.
 * @return True if transaction was successful. False if EEPROM was already full.
 */
bool write_flash(int16_t value) {
    if (data_write_counter < DATA_ARRAY_SIZE) { // check if end of eeprom reached
        eeprom_base_ptr[data_write_counter++] = value;
        eeprom_base_ptr[FLASH_COUNTER_POINTER] = data_write_counter;  //store next pointer position in last array element
        return true;
    }
    return false;
}

/**
 * @brief Reset the EEPROM/flash by clearing the write pointer.
 *
 * Clears the variable as well as the stored variable in EEPROM.
 */
void flash_reset() {
    // Store an optional backup END marker so we are able to recover the data even if the pointer was already cleared.
    eeprom_base_ptr[data_write_counter] = -32768;
    data_write_counter = 0;
    eeprom_base_ptr[FLASH_COUNTER_POINTER] = data_write_counter;
}

/**
 * @brief Extract the stored values from EEPROM and print them over USART
 *
 * ::flash_setup and ::usart_setup has to be called prior to this function.
 * @return Number of downloaded values
 */
uint16_t download_flash_to_usart() {
    int16_t i = 0;
    while (i < data_write_counter) {
        int16_t message = eeprom_base_ptr[i];

        char message_buffer[(sizeof(int16_t) * 8)];
        sprintf(message_buffer, "%d\r\n", message);

        for (uint8_t j = 0; j < strlen(message_buffer); j++) {
            while (!(USART2->ISR & USART_ISR_TXE)) {
                // wait until the TDR register has been read
            }
            USART2->TDR = message_buffer[j] & USART_TDR_TDR_Msk;
        }
        i++;
    }
    return i;
}

/**
 * @brief Sets up the USART interface for transmission only.
 *
 * Copied from Labor Datentechnik and modified to remove receive logic.
 */
void usart_setup() {
    // enable peripheral clock for port A
    RCC->IOPENR |= RCC_IOPENR_IOPAEN;

    // set alternate functions for PA2 and PA3 to AF4 (USART2)
    GPIOA->AFR[0] = (GPIOA->AFR[0] & ~(GPIO_AFRL_AFSEL2_Msk | GPIO_AFRL_AFSEL3_Msk))
            | ((4 << GPIO_AFRL_AFSEL2_Pos) & GPIO_AFRL_AFSEL2_Msk) | ((4 << GPIO_AFRL_AFSEL3_Pos) & GPIO_AFRL_AFSEL3_Msk);

    // set GPIO mode for PA2 and PA3 to alternate function (mode 2)
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE2_Msk | GPIO_MODER_MODE3_Msk))
            | ((2 << GPIO_MODER_MODE2_Pos) & GPIO_MODER_MODE2_Msk) | ((2 << GPIO_MODER_MODE3_Pos) & GPIO_MODER_MODE3_Msk);

    // enable peripheral clock for USART2
    RCC->APB1ENR |= RCC_APB1ENR_USART2EN;

    // set baudrate to 115200
    USART2->BRR = 16000000 / 115200;
    // enable USART2 receiver, transmitter, RX interrupt and USART
    USART2->CR1 |= (USART_CR1_RE | USART_CR1_TE | USART_CR1_UE);
}
