/*
 * @file eeprom.h
 */

#ifndef EEPROM_H_
#define EEPROM_H_

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stm32l073xx.h>

void flash_setup();
bool write_flash(int16_t value);
void flash_reset();
uint16_t download_flash_to_usart();
void usart_setup();

#endif /* EEPROM_H_ */
