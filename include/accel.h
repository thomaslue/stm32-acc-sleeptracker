/*
 * accel.h
 *
 *  Created on: 30 Dec 2020
 *      Author: thomas
 */

#ifndef ACCEL_H_
#define ACCEL_H_

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "lis3dh_reg.h"
#include "delay.h"
#include "i2c.h"

stmdev_ctx_t dev_ctx;

bool has_double_tapped();
void accel_setup();
void setup_double_tap_detection();
bool get_new_accel_abs(uint16_t* result);

#endif /* ACCEL_H_ */
