# "Schlafüberwachung" mit Lagesensor

## Setting
Sensor ist in Matraze eingebaut, ist also normalerweise komplett still und soll bei Schlaf die Bewegung erfassen.

## Unsere Ziele
- [x] Änderungsrate der Bewegung während des Schlafs erfassen (mit Mittelwert auf mC o.Ä.)
- [x] Schlaf Beginn und Ende kennzeichnen mit Klick auf Buttons
- [x] Daten speichern und bei Verbindung mit PC ausgeben
- [x] _Optional_ Zusätzliche Ausgabe über Display (Aktivieren über Doppeltipp auf Sensor)
- [ ] _Optional_ Automatische Erkennung von Beginn (einfach) und Ende (schwerer) => Nicht möglich, da der Sensor zu doll rauscht und der Unterschied dafür zu gering ist.
